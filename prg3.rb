# Join two arrays without using inbuilt method

require 'pry'
arr = [1,2,3,4, 5]
brr = [5,6,7,8,9]

crr = []
i = 0
arr.each do
  crr << arr[i]
  i += 1
end

k = 0
brr.each do
  crr << brr[k]
  k += 1
end
p crr   # merge array
