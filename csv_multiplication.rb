require "csv"
require 'pry'

new_array = []
CSV.foreach('multiply.csv') do |row|
  value_first = row[0]
  value_second = row[1]
  sum = value_first.to_i * value_second.to_i
  new_array << [value_first, value_second, sum]
end

# headers = ["val1", "val2", "mul"]
CSV.open("multiply.csv", 'w') do |csv|
  # headers ||= csv.headers
  new_array.each do |arr|
    csv << arr
  end
end
