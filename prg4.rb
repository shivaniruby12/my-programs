# Write a method to double all the elements in an array.

def double_arr_elements
  arr = [1, 3, 4, 6]
  arr.map{ |a| a * 2}
end

puts double_arr_elements
