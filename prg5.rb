# method to double all the elements in the array. However, handle edge cases (like array can have a character) as well.

def double_arr_elements
  arr = [1, 3, 4, 6, "ccc", "shi", 5]
  arr.map{ |a| a * 2 if a.is_a? Integer}.compact
end

puts double_arr_elements
