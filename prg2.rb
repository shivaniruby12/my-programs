# Check if a value exists in an array

require 'pry'

print "Enter element :"
num = gets.chomp
flag = 0

arr = [2, 4, 6, 7, 'apple', 'orange']

arr.each do |a|
  if a == num
    puts "Value exists"
    flag  = true
  end
end

if flag == true
  puts "#{num} exists in array"
else
  puts "#{num} does not exists in array"
end
